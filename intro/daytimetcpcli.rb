require 'socket'

abort("usage: #{$0} <IPaddress>") unless ARGV.count == 1

socket = Socket.new(:AF_INET, :SOCK_STREAM, 0)
socket.connect(Socket.sockaddr_in(9999, ARGV.first))
puts socket.read
