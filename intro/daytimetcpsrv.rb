require 'socket'

listen_socket = Socket.new(Socket::AF_INET, Socket::SOCK_STREAM, 0)
listen_socket.bind(Socket.sockaddr_in(9999, ''))
listen_socket.listen(10_000)

loop do
  connection = listen_socket.accept.first
  connection.write("#{Time.now.ctime}\r\n")
  connection.close
end
