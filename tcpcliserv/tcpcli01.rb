require 'socket'
require_relative 'tcpcliserv'

abort("usage: #{$0} <IPaddress>") unless ARGV.count == 1

socket = Socket.new(:AF_INET, :SOCK_STREAM, 0)
socket.connect(Socket.sockaddr_in(SERV_PORT, ARGV.first))

str_cli(STDIN, socket)
