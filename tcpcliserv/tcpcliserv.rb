SERV_PORT = 9877
LISTENQ = 1024
MAXLINE = 4096

def str_echo(conn)
  loop { conn << conn.readpartial(MAXLINE) }
rescue EOFError
end

def str_cli(io, socket)
  loop do
    socket << io.readpartial(MAXLINE)
    output = socket.readpartial(MAXLINE)
    abort("str_cli: server terminated prematurely") if output.empty?
    print output
  end
rescue EOFError
end
