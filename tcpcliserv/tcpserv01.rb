require 'socket'
require_relative 'tcpcliserv'

def main
  listen_socket = Socket.new(:AF_INET, :SOCK_STREAM, 0)
  listen_socket.bind(Socket.sockaddr_in(SERV_PORT, ''))
  listen_socket.listen(LISTENQ)

  loop do
    conn, _ = listen_socket.accept
    fork do
      listen_socket.close
      str_echo(conn)
    end
    conn.close
  end
end

main
